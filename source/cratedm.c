/*
	3ZB2-CrateDM mod by morry
*/

#include "g_local.h"

// code taken from FunNames by Ryan "Choncha" Nunn
char *CrateDM_MakeGreen(char *in)
{
	int i = 0;
	char *out = gi.TagMalloc(sizeof(in), TAG_LEVEL);

	for (i = 0; in[i]; i++)
	{
		if (in[i] != '\n')
		{
			out[i] = in[i] + 128;
		}
		else
		{
			out[i] = in[i];
		}
	}

	out[i] = 0;
	return out;
}

void CrateDM_Precache(void)
{
	gi.imageindex("c_brown");
	gi.imageindex("c_crate");
	gi.imageindex("c_explode");
	gi.imageindex("c_grey");
	gi.imageindex("c_red");
	gi.imageindex("c_wood1");
	gi.imageindex("c_wood2");
	gi.imageindex("c_wood3");
}

void CrateDM_AssignSkin(edict_t *ent)
{
	int playernum = ent-g_edicts-1;
	char t[64];

	strcpy(t, "crate/");

	switch (ent->client->resp.crateskin)
	{
		case SKIN_BROWN:
			gi.configstring(CS_PLAYERSKINS+playernum, va("%s\\%s%s", ent->client->pers.netname, t, SKINNAME_BROWN));
			break;
		case SKIN_EXPLODE:
			gi.configstring(CS_PLAYERSKINS+playernum, va("%s\\%s%s", ent->client->pers.netname, t, SKINNAME_EXPLODE));
			break;
		case SKIN_GREY:
			gi.configstring(CS_PLAYERSKINS+playernum, va("%s\\%s%s", ent->client->pers.netname, t, SKINNAME_GREY));
			break;
		case SKIN_RED:
			gi.configstring(CS_PLAYERSKINS+playernum, va("%s\\%s%s", ent->client->pers.netname, t, SKINNAME_RED));
			break;
		case SKIN_WOOD1:
			gi.configstring(CS_PLAYERSKINS+playernum, va("%s\\%s%s", ent->client->pers.netname, t, SKINNAME_WOOD1));
			break;
		case SKIN_WOOD2:
			gi.configstring(CS_PLAYERSKINS+playernum, va("%s\\%s%s", ent->client->pers.netname, t, SKINNAME_WOOD2));
			break;
		case SKIN_WOOD3:
			gi.configstring(CS_PLAYERSKINS+playernum, va("%s\\%s%s", ent->client->pers.netname, t, SKINNAME_WOOD3));
			break;
		case SKIN_CRATE:
		default:
			ent->client->resp.crateskin = SKIN_CRATE;
			gi.configstring(CS_PLAYERSKINS+playernum, va("%s\\%s%s", ent->client->pers.netname, t, SKINNAME_CRATE));
			break;
	}
}

void CrateDM_ChangeSkin(edict_t *ent)
{
	char *t;
	int desired_skin;

	t = gi.args();
	if (!*t)
	{
		if (!G_EntIsBot(ent))
		{
			gi.cprintf(ent, PRINT_HIGH, "You are currently the %s crate.\n", CrateDM_MakeGreen(CrateDM_SkinName(ent->client->resp.crateskin)));
		}

		return;
	}

	if (Q_stricmp(t, "brown") == 0)
		desired_skin = SKIN_BROWN;
	else if (Q_stricmp(t, "crate") == 0)
		desired_skin = SKIN_CRATE;
	else if (Q_stricmp(t, "explode") == 0)
		desired_skin = SKIN_EXPLODE;
	else if (Q_stricmp(t, "grey") == 0)
		desired_skin = SKIN_GREY;
	else if (Q_stricmp(t, "red") == 0)
		desired_skin = SKIN_RED;
	else if (Q_stricmp(t, "wood1") == 0)
		desired_skin = SKIN_WOOD1;
	else if (Q_stricmp(t, "wood2") == 0)
		desired_skin = SKIN_WOOD2;
	else if (Q_stricmp(t, "wood3") == 0)
		desired_skin = SKIN_WOOD3;
	else if (Q_stricmp(t, "random") == 0)
	{
		// randomize skin number
		desired_skin = 1 + (rand() % 8);

		// some wicked sanity checks, huh? shouldn't happen..
		if (desired_skin <= SKIN_NONE)
			desired_skin = SKIN_BROWN;
		else if (desired_skin >= SKIN_MAX)
			desired_skin = SKIN_WOOD3;

		// desired skin is current skin, decrease or increase skin number
		if (ent->client->resp.crateskin == desired_skin)
		{
			if (desired_skin == SKIN_WOOD3)
				desired_skin--;
			else
				desired_skin++;
		}
	}
	else
	{
		if (!G_EntIsBot(ent))
		{
			gi.cprintf(ent, PRINT_HIGH, "Unsupported skin %s.\n", t);
		}

		return;
	}

	if (ent->client->resp.crateskin == desired_skin)
	{
		if (!G_EntIsBot(ent))
		{
			gi.cprintf(ent, PRINT_HIGH, "You are already dressed as the %s crate.\n", CrateDM_MakeGreen(CrateDM_SkinName(desired_skin)));
		}

		return;
	}

	// change the skin
	ent->client->resp.crateskin = desired_skin;
	CrateDM_AssignSkin(ent);

	if (!G_EntIsBot(ent))
	{
		gi.cprintf(ent, PRINT_HIGH, "You are now the %s crate.\n", CrateDM_MakeGreen(CrateDM_SkinName(desired_skin)));
	}

	// add a teleportation effect
	ent->s.event = EV_PLAYER_TELEPORT;
}

char *CrateDM_SkinName(int skin)
{
	switch (skin)
	{
		case SKIN_BROWN:
			return "Brown";
		case SKIN_CRATE:
			return "Crate";
		case SKIN_EXPLODE:
			return "Explode";
		case SKIN_GREY:
			return "Grey";
		case SKIN_RED:
			return "Red";
		case SKIN_WOOD1:
			return "Wood1";
		case SKIN_WOOD2:
			return "Wood2";
		case SKIN_WOOD3:
			return "Wood3";
	}

	return "Unknown";
}

char *CrateDM_IconName(int skin)
{
	switch (skin)
	{
		case SKIN_BROWN:
			return "c_brown";
		case SKIN_CRATE:
			return "c_crate";
		case SKIN_EXPLODE:
			return "c_explode";
		case SKIN_GREY:
			return "c_grey";
		case SKIN_RED:
			return "c_red";
		case SKIN_WOOD1:
			return "c_wood1";
		case SKIN_WOOD2:
			return "c_wood2";
		case SKIN_WOOD3:
			return "c_wood3";
	}

	return "i_fixme";
}

void CrateDM_SpawnBotChangeSkin(edict_t *self)
{
	if (!self->crateskin)
	{
		gi.dprintf("bot_changeskin: without crateskin set at %s\n", vtos(self->s.origin));
		G_FreeEdict(self);
		return;
	}
	
	if (self->crateskin <= SKIN_NONE || self->crateskin >= SKIN_MAX)
	{
		self->crateskin = 2;
		gi.dprintf("bot_changeskin: incorrect skin number set at %s, defaulted to %i\n", vtos(self->s.origin), self->crateskin);
	}
}

// taken from the CTF source by Zoid
static edict_t *cratedm_findradius(edict_t *from, vec3_t org, float rad)
{
	vec3_t	eorg;
	int		j;

	if (!from)
		from = g_edicts;
	else
		from++;
	for ( ; from < &g_edicts[globals.num_edicts]; from++)
	{
		if (!from->inuse)
			continue;
		for (j = 0; j < 3; j++)
			eorg[j] = org[j] - (from->s.origin[j] + (from->mins[j] + from->maxs[j])*0.5);
		if (VectorLength(eorg) > rad)
			continue;
		return from;
	}

	return NULL;
}

void CrateDM_BotChangeSkin(edict_t *bot)
{
	edict_t *target = NULL;

	if (!bot || !bot->client)
	{
		return;
	}

	if (!G_EntIsBot(bot))
	{
		return;
	}

	if (bot->health <= 0)
	{
		return;
	}

	// are we near a bot_changeskin item?
	while ((target = cratedm_findradius(target, bot->s.origin, 128)) != NULL)
	{
		if (target->classname && (Q_stricmp(target->classname, "bot_changeskin") == 0))
		{
			if (target->crateskin && bot->client->resp.crateskin != target->crateskin)
			{
				// change the bot skin
				bot->client->resp.crateskin = target->crateskin;
				CrateDM_AssignSkin(bot);

				// add a teleportation effect
				bot->s.event = EV_PLAYER_TELEPORT;
			}
		}
	}
}
