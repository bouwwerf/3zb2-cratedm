/*
	3ZB2-CrateDM mod by morry
*/

// skin num
typedef enum {
	SKIN_NONE,
	SKIN_BROWN,
	SKIN_CRATE,
	SKIN_EXPLODE,
	SKIN_GREY,
	SKIN_RED,
	SKIN_WOOD1,
	SKIN_WOOD2,
	SKIN_WOOD3,
	SKIN_MAX
} crateskin_t;

// supported skins
#define SKINNAME_BROWN "brown"
#define SKINNAME_CRATE "crate"
#define SKINNAME_EXPLODE "explode"
#define SKINNAME_GREY "grey"
#define SKINNAME_RED "red"
#define SKINNAME_WOOD1 "wood1"
#define SKINNAME_WOOD2 "wood2"
#define SKINNAME_WOOD3 "wood3"

// functions
void CrateDM_Precache(void);
void CrateDM_AssignSkin(edict_t *ent);
void CrateDM_ChangeSkin(edict_t *ent);
char *CrateDM_SkinName(int skin);
char *CrateDM_IconName(int skin);
void CrateDM_BotChangeSkin(edict_t *bot);
