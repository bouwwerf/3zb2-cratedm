3ZB2-CrateDM r1 by morry

This modification adds some more fun to the original "CrateDM" by Chris 'Shatter' Holden.

His readme.txt stated the following as future improvements (which are our goals):
"I would like to add a lot of new crate loaded maps with various crate colors and such so it becomes a more challenging play.
Since there are several crate skins (just the id crate textures) binds could be included so that you could change your skin to fit your setting.
Sounds for the crate instead of the male player sounds possibly."

Features in 3ZB2-CrateDM:
- Players are forced to use the Crate model and skin.
- Players must change skin with "cmd crate" followed by one of these options:
	1. "brown" for the brown crate.
	2. "crate" for the green crate.
	3. "explode" for the exploding crate.
	4. "grey" for the grey crate.
	5. "red" for the red crate.
	6. "wood1" for the first wooden crate variant.
	7. "wood2" for the second wooden crate variant.
	8. "wood3" for the third wooden crate variant.
	9. "random" for a random crate variant.
- Player's HUD will show which skin is inuse. The icon will be shown infront of the number of frags.
- Bots may change skin if the surrounding crates are of different material. For mappers: use "bot_changeskin" item with "crateskin" value 1 to 8 (see cmd crate information).
- A second map added: cratedm2 "Morry Says Hide"