140498
==============================================================
CrateDM Q2 - made today
--------------------------------------------------------------

by: Chris Holden - tossed all of the crap together
  (shatter@planetquake.com)             
==============================================================

Overview:

CrateDM pits opponents against each other in a room full of crates, and the players are crates.
Later on one day, I was experimenting with some PPM stuff while I was working on a new one, and I made a crate ppm. Now, this was just for testing things in the way of ppm creation, but I laughed my ass off watching it move around the map. I considered the possibility of DM one on one with both players using this crate ppm in a map with a lot of crates, thus you could spot running and "blend in" so to speak. I didn't really see any of the q2 maps that had the number of crates that I was looking for, so I spent around less than 20 minutes and tossed together a single room map. This room was just a large room with a LOT of crates and weapons/items. I sent the mod over to a friend and we played for several hours having a great time with it. The gameplay was insane, since the whole time it's just funny to see a crate sliding around the map, and then when you're mid battle, all of the sudden, your opponent disappears. It was extremely fun and creepy, so i figured, why not release it?
But first of all, I warn you, this is VERY simple, I made this entire thing in under an hour, the crate ppm is nothing more than a crate, period, don't expect anything flashy in this. It's just good fun with a dumb idea. :)

This is also real fun with bots, since it's harder to see them, but they can see you anywhere.


  
Features:

None really.

Installation:

Unzip the CrateDM stuff with subdirectories into your quake2 directory and run Cratedm1 (make sure both players are using the crate/crate skin, so they match the included map).

Future plans:

This thing is really stupid, but like I said, fun. I have been playing it all day, and would like to extend it a bit. I would like to add a lot of new crate loaded maps with various crate colors and such so it becomes a more challenging play. Since there are several crate skins (just the id crate textures) binds could be included so that you could change your skin to fit your setting. Sounds for the crate instead of the male player sounds possibly.


Anyhow, just enjoy it and don't abuse it. BTW, if you're thinking of taking the crate out and using it in normal DM to try to trick people, remember, if THEY don't have to model you look like the male model, and in any other map, you stick out just like any other player model, it's only camo in one of these mass crate rooms (you'll have to see it work to understand).

